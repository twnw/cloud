package enet.imitate.core;

import enet.imitate.common.Constants;
import enet.imitate.common.Log;
import enet.imitate.common.Utils;
import enet.imitate.event.EC;
import enet.imitate.event.EL;
import enet.imitate.event.EP;
import org.slf4j.ILoggerFactory;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static enet.imitate.common.Utils.findMethod;
import static enet.imitate.common.Utils.getPid;
import static enet.imitate.common.Utils.isNotEmpty;

/**
 * 系统环境
 */
public class Environment {
    public final static String                           PROP_ACTIVE      = "env.profiles.active";
    protected           Log                              log              = Log.of(Environment.class);
    protected EP ep;
    /**
     * 配置文件路径
     */
    protected final List<String> cfgFileLocations = new LinkedList<>();
    /**
     * 配置文件名字
     */
    protected           List<String>                     cfgFileNames     = new LinkedList<>();
    /**
     * 最终汇总属性
     */
    protected final Map<String, String> finalAttrs       = new ConcurrentHashMap<>();
    /**
     * 运行时改变的属性, 优先级高于 {@link #finalAttrs} see: {@link #getAttr(String)}
     */
    protected final     Map<String, String>              runtimeAttrs     = new ConcurrentHashMap<>(7);
    /**
     * location 和 其对应的 所有属性
     */
    protected final     Map<String, Map<String, String>> locationSources  = new LinkedHashMap<>();
    /**
     * profile 和 其对应的 所有属性
     */
    protected final     Map<String, Map<String, String>> profileSources   = new LinkedHashMap<>();
    /**
     * 所有的profile
     */
    protected final Set<String> allProfiles      = new HashSet<>(7);
    /**
     * 激活特定配置
     */
    protected final     Set<String>                      activeProfiles   = new LinkedHashSet<>(5);

    public Environment() {
        // 按优先级添加. 先添加优先级最低, 越后边越高
        cfgFileLocations.add("classpath:/");
        cfgFileLocations.add("classpath:/config/");
        cfgFileLocations.add("file:./");
        cfgFileLocations.add("file:./config/");
        String p = System.getProperty(Constants.CFG_FILELOCATIONS);
        if (p != null && !p.isEmpty()) {
            for (String s : p.split(Constants.COMMA)) {
                if (s != null && !s.trim().isEmpty()) cfgFileLocations.add(s.trim());
            }
        }

        cfgFileNames.add(Constants.APPLICATION);
        p = System.getProperty(Constants.CFG_FILENAME );
        if (p != null && !p.isEmpty()) {
            for (String s : p.split(Constants.COMMA)) {
                if (s != null && !s.trim().isEmpty()) cfgFileNames.add(s.trim());
            }
        }
    }

    /**
     * 加载配置
     * 支持: properties 文件
     */
    protected void loadCfg() {
        log.trace("start loading cgf file");
        // 先加载默认配置文件
        for (String l : cfgFileLocations) {
            if (l == null || l.isEmpty()) continue;
            for (String n : cfgFileNames) {
                loadPropertiesFile(null, l, n);
                // loadYamlFile(null, l, n);
            }
        }
        // 遍历加载所有profile对应的配置文件
        Set<String> loadedProfiles = new HashSet<>();
        while (allProfiles.size() > loadedProfiles.size()) {
            for (String p : allProfiles) {
                if (loadedProfiles.contains(p)) continue;
                loadedProfiles.add(p); // 放在上面, 以免加载出错, 导致死循环
                for (String l : cfgFileLocations) {
                    if (l == null || l.isEmpty()) continue;
                    for (String n : cfgFileNames) {
                        loadPropertiesFile(p, l, n);
                        // loadYamlFile(p, l, n);
                    }
                }
            }
        }
        if (isNotEmpty(System.getProperty(PROP_ACTIVE))) {
            activeProfiles.clear();
            activeProfiles.addAll(Arrays.asList(System.getProperty(PROP_ACTIVE).split(Constants.COMMA)));
        }
        if (!profileSources.isEmpty()) finalAttrs.putAll(profileSources.get(null));

        for (String p : activeProfiles) {
            if (profileSources.containsKey(p)) finalAttrs.putAll(profileSources.get(p));
        }
        finalAttrs.put(PROP_ACTIVE, activeProfiles.stream().collect(Collectors.joining(Constants.COMMA)));
        parseValue(finalAttrs, new AtomicInteger(0));
        log.debug("final attrs: {}", finalAttrs);
        log.info("The following profiles are active: {}", finalAttrs.get(PROP_ACTIVE));
        // 初始化日志相关
        Log.init(() -> initLog());
        ep.fire(Constants.ENV_CONFIGURED, EC.of(this));
    }
    /**
     * 初始化日志实现系统
     */
    protected void initLog() {
        ILoggerFactory fa = LoggerFactory.getILoggerFactory();
        if ("ch.qos.logback.classic.LoggerContext".equals(fa.getClass().getName())) {
            try {
                // 设置logback 配置文件中的变量
                System.setProperty(Constants.PID, getPid());
                String logPath = getAttr("log.path");
                if (isNotEmpty(logPath)) System.setProperty("LOG_PATH", logPath);
                Object o = Class.forName("ch.qos.logback.classic.joran.JoranConfigurator").newInstance();
                Method m = findMethod(o.getClass(), "setContext", Class.forName("ch.qos.logback.core.Context"));
                m.invoke(o, fa);
                m = findMethod(fa.getClass(), "reset"); m.invoke(fa);
                m = findMethod(o.getClass(), "doConfigure", InputStream.class);
                boolean f = false;
                for (String p : activeProfiles) {
                    InputStream in = getClass().getClassLoader().getResourceAsStream("logback-" + p + ".xml");
                    if (in != null) { m.invoke(o, in); f = true; }
                }
                // 设置日志级别
                Method setLevel = findMethod(Class.forName("ch.qos.logback.classic.Logger"), "setLevel", Class.forName("ch.qos.logback.classic.Level"));
                Method toLevel = findMethod(Class.forName("ch.qos.logback.classic.Level"), "toLevel", String.class);
                for (Map.Entry<String, String> e : groupAttr(Constants.LOG_LEVEL).entrySet()) {
                    setLevel.invoke(fa.getLogger(e.getKey()), toLevel.invoke(null, e.getValue()));
                }
            } catch (Exception e) {
                log.error(e);
            }
        }
    }
    /**
     * 取一组属性集合
     * @param keys 属性前缀
     * @return
     */
    @EL(name = Constants.ENV_NS, async = false)
    public Map<String, String> groupAttr(String... keys) {
        Map<String, String> group = new HashMap<>();
        if (keys == null) return group;
        BiConsumer<String, String> fn = (k, v) -> {
            for (String key : keys) {
                if (!k.startsWith(key)) continue;
                if (k.equals(key)) group.put(k, v);
                else group.put(k.substring(key.length() + 1), v);
            }
        };
        finalAttrs.forEach(fn);
        System.getProperties().forEach((k, v) -> fn.accept(k.toString(), Objects.toString(v, null)));
        runtimeAttrs.forEach(fn);
        return group;
    }

    /**
     * 取属性.
     * @param key
     * @return
     */
    @EL(name = "env.getAttr", async = false)
    protected String getAttr(String key) {
        String v = runtimeAttrs.get(key);
        if (v == null) v = System.getProperty(key);
        if (v == null) v = finalAttrs.get(key);
        return v;
    }

    /**
     * 用于运行时改变属性
     * @param key
     * @param value
     * @return
     */
    public Environment setAttr(String key, String value) {
        if (PROP_ACTIVE.equals(key)) throw new RuntimeException("not allow change this property '" + PROP_ACTIVE + "'");
        ep.fire(
                Constants.ENV_UPDATEATTR,
                EC.of(this).args(key, value),
                ec -> {
                    if (ec.isSuccess()) runtimeAttrs.put(key, value);
                }
        );
        return this;
    }

    protected Pattern p = Pattern.compile("(\\$\\{(?<attr>[\\w\\._]+)\\})+");
    /**
     * 替换 值包含 ${attr}的字符串
     * @param attrs
     */
    protected void parseValue(Map<String, String> attrs, AtomicInteger count) {
        if (count.get() >= 3) return;
        boolean f = false;
        count.getAndIncrement();
        for (Map.Entry<String, String> e : attrs.entrySet()) {
            Matcher m = p.matcher(e.getValue());
            if (!m.find()) continue;
            f = true;
            attrs.put(e.getKey(), e.getValue().replace(m.group(0), attrs.getOrDefault(m.group(Constants.ATTR), Constants.EMPTY)));
        }
        if (f) parseValue(attrs, count); // 一直解析直到所有值都被替换完成;
    }

    public Integer getInteger(String key, Integer defaultValue) {
        String v = getAttr(key);
        return Utils.toInteger(v, defaultValue);
    }


    public Long getLong(String key, Long defaultValue) {
        String v = getAttr(key);
        return Utils.toLong(v, defaultValue);
    }


    public String getString(String key, String defaultValue) {
        String v = getAttr(key);
        return (v == null ? defaultValue : v);
    }


    public Boolean getBoolean(String name, Boolean defaultValue) {
        String v = getAttr(name);
        return Utils.toBoolean(v, defaultValue);
    }


    /**
     * 加载 properties 文件源
     * @param profile
     * @param cfgFileLocation
     * @param cfgFileName
     */
    protected void loadPropertiesFile(String profile, String cfgFileLocation, String cfgFileName) {
        String f = cfgFileLocation + cfgFileName + (profile == null ? Constants.EMPTY : "-" + profile) + Constants.DOT_PROPERTIES;

        Map<String, String> r = null;
        if (cfgFileLocation.startsWith("classpath:")) {
            try (InputStream in = getClass().getClassLoader().getResourceAsStream(f.replace("classpath:/", ""))){
                if (in != null) {
                    Properties p = new Properties(); p.load(in);
                    r = new LinkedHashMap(p);
                }
            } catch (Exception e) {
                log.error(e, "load cfg file '{}' error", f);
            }
        } else {
            try (InputStream in = new URL(f).openStream()) {
                Properties p = new Properties(); p.load(in);
                r = new LinkedHashMap(p);
            } catch (FileNotFoundException e) {
                log.trace("not found cfg file '{}'", f);
            } catch (Exception e) {
                log.error(e, "load cfg file '{}' error", f);
            }
        }
        if (r == null) return;
        log.trace("load cfg file '{}'\n{}", f, r);
        locationSources.put(f, r);
        profileSources.computeIfAbsent(profile, s -> new LinkedHashMap<>()).putAll(r);
        if (r.containsKey(PROP_ACTIVE)) {
            activeProfiles.clear();
            for (String p : r.get(PROP_ACTIVE).split(",")) {
                if (Utils.isNotBlank(p)) activeProfiles.add(p.trim());
            }
            allProfiles.addAll(activeProfiles);
        }
    }

    public Environment setEp(EP ep) {
        this.ep = ep;
        ep.addListenerSource(this);
        return this;
    }

    public Map<String, String> getFinalAttrs() {
        return new HashMap<>(finalAttrs); // 不能被外部改变
    }


    public Map<String, Map<String, String>> getLocationSources() {
        return new LinkedHashMap<>(locationSources); // 不能被外部改变
    }
}
