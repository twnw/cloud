package ent.imitate.server.dao.hibernate;

import java.lang.annotation.*;

/**
 * @Description TODO  </br>
 * @Author tonywang</br>
 * @DATE 4/17/2019 2:09 PM</br>
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Repo {
}
