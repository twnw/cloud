package ent.imitate.server.dao.hibernate;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.MappedSuperclass;
import java.util.Date;

/**
 * @Description TODO  </br>
 * @Author tonywang</br>
 * @DATE 4/17/2019 2:12 PM</br>
 */
@MappedSuperclass
@DynamicUpdate
public class BaseEntity  implements IEntity {
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;


    public Date getCreateTime() {
        return createTime;
    }


    public BaseEntity setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }


    public Date getUpdateTime() {
        return updateTime;
    }


    public BaseEntity setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
        return this;
    }
}
