package ent.imitate.server.resteasy;

import java.lang.annotation.*;

/**
 * @Description TODO  </br>
 * @Author tonywang</br>
 * @DATE 4/16/2019 2:46 PM</br>
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SessionId {
}
