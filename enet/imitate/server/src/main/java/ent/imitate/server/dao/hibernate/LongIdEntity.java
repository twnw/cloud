package ent.imitate.server.dao.hibernate;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * @Description TODO  </br>
 * @Author tonywang</br>
 * @DATE 4/17/2019 2:59 PM</br>
 */
@MappedSuperclass
public class LongIdEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    public Long getId() {
        return id;
    }


    public LongIdEntity setId(Long id) {
        this.id = id;
        return this;
    }
}