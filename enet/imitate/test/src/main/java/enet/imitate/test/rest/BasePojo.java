package enet.imitate.test.rest;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * @Description TODO  </br>
 * @Author tonywang</br>
 * @DATE 4/18/2019 2:09 PM</br>
 */
public class BasePojo implements Serializable {
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }
}
