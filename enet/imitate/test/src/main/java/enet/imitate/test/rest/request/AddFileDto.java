package enet.imitate.test.rest.request;

import enet.imitate.test.rest.BasePojo;
import enet.imitate.test.rest.FileData;

/**
 * @Description TODO  </br>
 * @Author tonywang</br>
 * @DATE 4/18/2019 2:11 PM</br>
 */
public class AddFileDto extends BasePojo {
    private String   name;
    private Integer  age;
    private FileData headportrait;


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public Integer getAge() {
        return age;
    }


    public void setAge(Integer age) {
        this.age = age;
    }


    public FileData getHeadportrait() {
        return headportrait;
    }


    public void setHeadportrait(FileData headportrait) {
        this.headportrait = headportrait;
    }
}
