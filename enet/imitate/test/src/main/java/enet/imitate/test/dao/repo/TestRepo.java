package enet.imitate.test.dao.repo;

import enet.imitate.test.dao.entity.TestEntity;
import ent.imitate.server.dao.hibernate.BaseRepo;
import ent.imitate.server.dao.hibernate.Repo;

@Repo
public class TestRepo extends BaseRepo<TestEntity, Long>{
}
