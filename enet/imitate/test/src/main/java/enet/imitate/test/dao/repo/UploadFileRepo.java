package enet.imitate.test.dao.repo;

import enet.imitate.test.dao.entity.UploadFile;
import ent.imitate.server.dao.hibernate.BaseRepo;

import java.util.Date;

/**
 * @Description TODO  </br>
 * @Author tonywang</br>
 * @DATE 4/18/2019 11:28 AM</br>
 */
public class UploadFileRepo extends BaseRepo<UploadFile, Long> {
    @Override
    public <S extends UploadFile> S saveOrUpdate(S e) {
        Date d = new Date();
        if (e.getCreateTime() == null) e.setCreateTime(d);
        if (e.getUpdateTime() == null) e.setCreateTime(d);
        return super.saveOrUpdate(e);
    }
}
