package enet.imitate.test;

import enet.imitate.common.Constants;
import enet.imitate.common.Log;
import enet.imitate.common.Utils;
import enet.imitate.core.AppContext;
import enet.imitate.event.EC;
import enet.imitate.event.EL;
import enet.imitate.test.common.Async;
import enet.imitate.test.common.Monitor;
import enet.imitate.test.dao.entity.TestEntity;
import enet.imitate.test.dao.repo.TestRepo;
import enet.imitate.test.rest.RestTpl;
import enet.imitate.test.service.FileUploader;
import enet.imitate.test.service.TestService;
import ent.imitate.server.ServerTpl;
import ent.imitate.server.cache.ehcache.EhcacheServer;
import ent.imitate.server.dao.hibernate.Hibernate;
import ent.imitate.server.dao.hibernate.Trans;
import ent.imitate.server.dao.hibernate.TransWrapper;
import ent.imitate.server.http.netty.NettyHttp;
import ent.imitate.server.mview.MViewServer;
import ent.imitate.server.redis.RedisServer;
import ent.imitate.server.resteasy.NettyResteasy;
import ent.imitate.server.sched.SchedServer;
import ent.imitate.server.session.MemSessionManager;
import ent.imitate.server.session.RedisSessionManager;
import ent.imitate.server.swagger.Controller;
import ent.imitate.server.swagger.OpenApiDoc;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;

import static enet.imitate.common.Utils.proxy;

/**
 * @Description TODO  </br>
 * @Author tonywang</br>
 * @DATE 4/15/2019 5:38 PM</br>
 */
public class TestServer extends ServerTpl {
        Log mLog = Log.of(getName());
    static {
        Log.init(null);
    }
    public static void main(String[] args) {
        AppContext appContext = new AppContext();
        appContext.addSource(new NettyHttp());
        appContext.addSource(new NettyResteasy().scan(RestTpl.class));
        appContext.addSource(new MViewServer());
        appContext.addSource(new Hibernate().scanEntity(TestEntity.class).scanRepo(TestRepo.class));
        appContext.addSource(new TransWrapper());
        appContext.addSource(new EhcacheServer());
        appContext.addSource(new RedisServer());
        appContext.addSource(new SchedServer());
        appContext.addSource(new RedisSessionManager());
        appContext.addSource(new OpenApiDoc());
        appContext.addSource(new Controller(new OpenApiDoc()));
        appContext.addSource(new TestServer());
        appContext.start();
    }

//    @EL(name = Constants.SYS_STARTING)
//    public void starting(){
//        mLog.info("系统启动 starting {}", "ing");
//    }
//    @EL(name = Constants.SYS_STOPPING)
//    public void stopping(){
//        mLog.info("系统关闭 stopping {0}", "ending");
//    }
//    @EL(name = Constants.SYS_STARTED)
//    public void started(){
//        mLog.info("系统启动完成 started {}", "stoped");
//    }
    @Resource
    AppContext ctx;
    @Resource
    Executor exec;
    /**
     * 环境配置完成后执行
     */
    @EL(name = Constants.ENV_CONFIGURED, async = false)
    private void envConfigured() {
        if (Utils.toBoolean(ep.fire("session.isEnabled"),false)) {
            String type = ctx.env().getString("session.type", "memory");
            if ("memory".equalsIgnoreCase(type)) {
                ctx.addSource(new MemSessionManager());
            } else if ("redis".equalsIgnoreCase(type)) {
                ctx.addSource(new RedisSessionManager());
            }
        }
        Function<Class<?>, ?> wrap = createAopFn();
        ctx.addSource(wrap.apply(TestService.class));
        ctx.addSource(wrap.apply(FileUploader.class));
    }
    /**
     * 创建aop函数
     * @return
     */
    protected Function<Class<?>, ?> createAopFn() {
        abstract class AopFn {
            abstract Object run(Method m, Object[] args, Supplier<Object> fn);
        }
        Map<Class, AopFn> aopFn = new HashMap<>();
        aopFn.put(Trans.class, new AopFn() {// 事务方法拦截
            @Override
            Object run(Method m, Object[] args, Supplier<Object> fn) {
                if (m.getAnnotation(Trans.class) != null) {
                    return bean(TransWrapper.class).trans(fn);
                } else return fn.get();
            }
        });
        aopFn.put(Async.class, new AopFn() {
            @Override
            Object run(Method m, Object[] args, Supplier<Object> fn) {
                if (m.getAnnotation(Async.class) != null) {
                    exec.execute(fn::get);
                    return null;
                } else return fn.get();
            }
        });
        aopFn.put(Monitor.class, new AopFn() {
            @Override
            Object run(Method m, Object[] args, Supplier<Object> fn) {
                Monitor monitorAnno = m.getAnnotation(Monitor.class);
                if (monitorAnno == null) return fn.get();

                long start = System.currentTimeMillis();
                Object ret = null;
                try {
                 ret = fn.get();
                } catch (Exception e) {
                    throw e;
                } finally {
                    long end = System.currentTimeMillis();
                    boolean warn = (end - start >= monitorAnno.warnTimeUnit().toMillis(monitorAnno.warnTimeOut()));
                    if (monitorAnno.trace() || warn){
                        StringBuilder sb = new StringBuilder(monitorAnno.logPrefix());
                        sb.append(m.getDeclaringClass().getName()).append(Constants.DOT).append(m.getName());
                        if (monitorAnno.printArgs() && args.length > 0) {
                            sb.append("(");
                            for (int i = 0; i < args.length; i++) {
                                String s;
                                if (args[i].getClass().isArray()) s = Arrays.toString((Object[]) args[i]);
                                else s = Objects.toString(args[i], "");

                                if (i == 0) sb.append(s);
                                else sb.append(";").append(s);
                            }
                            sb.append(")");
                            }
                        sb.append(", time: ").append(end - start).append("ms");
                        if (warn) log.warn(sb.append(monitorAnno.logSuffix()).toString());
                        else log.info(sb.append(monitorAnno.logSuffix()).toString());
                        }
                    }
                    return ret;
                }
        });

        // 多注解拦截
        return new Function<Class<?>, Object>() {
            @Override
            public Object apply(Class<?> clz) {
                Method m = Utils.findMethod(clz, mm ->
                        mm.getAnnotation(Trans.class) != null || mm.getAnnotation(Async.class) != null
                );
                if (m == null) {
                    try {
                        return clz.newInstance();
                    } catch (Exception e) {
                        log.error(e);
                    }
                    return null;
                }
                return proxy(clz, (obj, method, args, proxy) -> {
                    Supplier fn = () -> {
                        try {
                            return proxy.invokeSuper(obj, args);
                        } catch (Throwable t) {
                            throw new RuntimeException(t);
                        }
                    };
                    return aopFn.get(Async.class).run(method, args, () -> aopFn.get(Monitor.class).run(method, args, () -> aopFn.get(Trans.class).run(method, args, fn)));
                });
            }
        };
    }

    /**
     * 系统启动结束后执行
     */
    @EL(name = "sys.started")
    private void sysStarted() {
        // ctx.stop(); // 测试关闭
    }

    @EL(name = "sched.started")
    private void schedStarted() {
        // 系统负载监控
        try {
            Field f = AppContext.class.getDeclaredField("exec");
            f.setAccessible(true);
            ThreadPoolExecutor v = (ThreadPoolExecutor) f.get(ctx);
            if (v instanceof ThreadPoolExecutor) {
                ep.fire("sched.cron", "0 0/1 * * * ?", (Runnable) () -> monitorExec(v));
            }
        } catch (Exception e) {
            log.error(e);
        }
    }

    /**
     * 监控系统线程池的运行情况
     * @param e
     */
    private void monitorExec(ThreadPoolExecutor e) {
        int size = e.getQueue().size();
        if (size > e.getCorePoolSize() * 50) {
            ep.fire("sys.load", EC.of(this).sync().args(size / 7));
            log.warn("system is very heavy load running!. {}", "[" + e.toString().split("\\[")[1]);
        } else if (size > e.getCorePoolSize() * 40) {
            ep.fire("sys.load", 8);
            log.warn("system is heavy load running. {}", "[" + e.toString().split("\\[")[1]);
            ep.fire("sched.after", 45, TimeUnit.SECONDS, (Runnable) () -> {
                if (e.getQueue().size() > size) {
                    ep.fire("sys.load", 50);
                    log.warn("system is heavy(up) load running. {}", "[" + e.toString().split("\\[")[1]);
                } else if (e.getQueue().size() < size) {
                    ep.fire("sys.load", 7);
                    log.warn("system is heavy(down) load running. {}", "[" + e.toString().split("\\[")[1]);
                }
            });
        } else if (size > e.getCorePoolSize() * 20) {
            ep.fire("sys.load", 5);
            log.warn("system will heavy load running. {}", "[" + e.toString().split("\\[")[1]);
            ep.fire("sched.after", 30, TimeUnit.SECONDS, (Runnable) () -> {
                if (e.getQueue().size() > size) {
                    ep.fire("sys.load", 20);
                    log.warn("system will heavy(up) load running. {}", "[" + e.toString().split("\\[")[1]);
                } else if (e.getQueue().size() < size) {
                    ep.fire("sys.load", 4);
                    log.warn("system will heavy(down) load running. {}", "[" + e.toString().split("\\[")[1]);
                }
            });
        } else if (size > e.getCorePoolSize() * 10) {
            log.warn("system is litter heavy load running. {}", "[" + e.toString().split("\\[")[1]);
            ep.fire("sched.after", 25, TimeUnit.SECONDS, (Runnable) () -> {
                if (e.getQueue().size() > size) {
                    ep.fire("sys.load", 7);
                    log.warn("system is litter heavy(up) load running. {}", "[" + e.toString().split("\\[")[1]);
                } else if (e.getQueue().size() < size) {
                    log.warn("system is litter heavy(down) load running. {}", "[" + e.toString().split("\\[")[1]);
                }
            });
        } else if (size > e.getCorePoolSize() * 5) {
            log.info("system executor: {}", "[" + e.toString().split("\\[")[1]);
        } else if (log.isDebugEnabled() || log.isTraceEnabled()) {
            log.debug("system executor: {}", "[" + e.toString().split("\\[")[1]);
        }
    }
}
