package enet.imitate.test.service;

import enet.imitate.test.common.Async;
import enet.imitate.test.dao.entity.TestEntity;
import enet.imitate.test.dao.entity.UploadFile;
import enet.imitate.test.dao.repo.TestRepo;
import enet.imitate.test.dao.repo.UploadFileRepo;
import enet.imitate.test.rest.PageModel;
import enet.imitate.test.rest.request.AddFileDto;
import ent.imitate.server.ServerTpl;
import ent.imitate.server.dao.hibernate.Trans;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Description TODO  </br>
 * @Author tonywang</br>
 * @DATE 4/18/2019 2:02 PM</br>
 */
public class TestService extends ServerTpl {
    @Resource
    TestRepo testRepo;
    @Resource
    UploadFileRepo uploadFileRepo;

    @Trans
    public PageModel findTestData() {
        TestEntity e = new TestEntity();
        e.setName("aaaa" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        e.setAge(111);
        testRepo.saveOrUpdate(e);
        return PageModel.of(
                testRepo.findPage(0, 5, (root, query, cb) -> {query.orderBy(cb.desc(root.get("id"))); return null;}),
                ee -> ee
        );
    }

    @Async
    @Trans
    public void save(AddFileDto dto) {
        UploadFile e = new UploadFile();
        e.setOriginName(dto.getHeadportrait().getOriginName());
        e.setThirdFileId(dto.getHeadportrait().getResultName());
        uploadFileRepo.saveOrUpdate(e);
    }
}
