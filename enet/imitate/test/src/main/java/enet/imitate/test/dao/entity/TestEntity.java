package enet.imitate.test.dao.entity;

import ent.imitate.server.dao.hibernate.LongIdEntity;

import javax.persistence.Entity;

/**
 * @Description TODO  </br>
 * @Author tonywang</br>
 * @DATE 4/18/2019 11:21 AM</br>
 */
@Entity
public class TestEntity extends LongIdEntity {
    private String name;
    private Integer age;


    public String getName() {
        return name;
    }


    public TestEntity setName(String name) {
        this.name = name;
        return this;
    }


    public Integer getAge() {
        return age;
    }


    public TestEntity setAge(Integer age) {
        this.age = age;
        return this;
    }
}
