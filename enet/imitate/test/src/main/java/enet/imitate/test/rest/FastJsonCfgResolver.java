package enet.imitate.test.rest;

import com.alibaba.fastjson.support.config.FastJsonConfig;
import enet.imitate.event.EP;
import io.swagger.v3.oas.models.OpenAPI;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ws.rs.ext.ContextResolver;

import static com.alibaba.fastjson.serializer.SerializerFeature.BrowserSecure;
import static com.alibaba.fastjson.serializer.SerializerFeature.WriteMapNullValue;

/**
 * @Description TODO  </br>
 * @Author tonywang</br>
 * @DATE 4/18/2019 3:51 PM</br>
 */
public class FastJsonCfgResolver implements ContextResolver<FastJsonConfig> {
    @Resource
    EP ep;
    FastJsonConfig cfg;

    @PostConstruct
    protected void init() {
        cfg = new FastJsonConfig();
        cfg.setSerializerFeatures(BrowserSecure, WriteMapNullValue);
    }

    @Override
    public FastJsonConfig getContext(Class<?> type) {
        if (OpenAPI.class.equals(type)) return new FastJsonConfig();
        return cfg;
    }
}
