package enet.imitate.common;

/**
 * @Description 常量  </br>
 * @Author tonywang</br>
 * @DATE 4/8/2019 1:53 PM</br>
 */
public class Constants {
    public final static String LOG = "log";
    public final static String APPLICATION = "application";
    public final static String PID = "PID";
    public final static String LOG_PATH = "LOG_PATH";
    public final static String LOG_LEVEL = "log.level";
    public final static String INIT_LOG = "enet.initlog";
    public final static String CFG_FILELOCATIONS = "enet.cfgFileLocations";
    public final static String CFG_FILENAME = "enet.cfgFileName";

    //http method
    public final static String GET = "GET";
    public final static String POST = "POST";
    public final static String CONTENT_TYPE = "Content-Type";
    public final static String COOKIE = "Cookie";
    public final static String SET_COOKIE = "Set-Cookie";

    public final static String TLS = "TLS";

    public final static String EQUAL = "=";
    public final static String SEMICOLON = ";";
    public final static String DOT = ".";
    public final static String COMMA = ",";
    public final static String DOT_CLASS = ".class";
    public final static String DOT_PROPERTIES = ".properties";
    public final static String JAR = "jar";
    public final static String EMPTY = "";
    public final static String SLASH = "/";
    public final static String AT = "@";
    public final static String SYS = "sys";
    public final static String ENV = "env";

    public final static String MULTIPART_FORM_DATA = "multipart/form-data";
    public final static String APPLICATION_JSON = "application/json";

    public final static String UTF_8 = "utf-8";
    public final static String ATTR = "attr";
    //系统启动
    public final static String SYS_STARTING = "sys.starting";
    //系统关闭
    public final static String SYS_STOPPING = "sys.stopping";
    //系统启动完成
    public final static String SYS_STARTED = "sys.started";
    //有属性改变
    public final static String ENV_UPDATEATTR = "env.updateAttr";
    //取一组属性集合
    public final static String ENV_NS = "env.ns";
    //查找bean
    public final static String BEAN_GET = "bean.get";
    public final static String SYS_BEAN_GET = "sys.bean.get";
    /**
     * 添加对象源
     */
    public final static String SYS_ADDSOURCE = "sys.addSource";
    /**
     * 环境已配置完成
     */
    public final static String ENV_CONFIGURED = "env.configured";

    public final static String SYS_INFO = "sys.info";
    /**
     * 线程池属性
     */
    public final static String SYS_EXEC = "sys.exec";
    public final static String SYS_EXEC_COREPOOLSIZE = "sys.exec.corePoolSize";
    public final static String SYS_EXEC_MAXIMUMPOOLSIZE = "sys.exec.maximumPoolSize";
    public final static String SYS_EXEC_KEEPALIVETIME = "sys.exec.keepAliveTime";
}
