package enet.imitate.common.builder;

import enet.imitate.common.Context;

/**
 * @Description 从一个运行上下文中, 计算出一个值  </br>
 * @Author tonywang</br>
 * @DATE 4/9/2019 4:22 PM</br>
 */
@FunctionalInterface
public interface Builder<T> {
    T build(Context ctx);
}
