package enet.imitate.common.devourer;

import enet.imitate.common.Log;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

/**
 * {@link Devourer} 管理器
 */
public class DevourerManager {
    protected final Log mLog = Log.of(DevourerManager.class);

    protected final Map<Object, Devourer> mDevourerMap = new ConcurrentHashMap<>(7);

    protected Executor mExecutor;

    public DevourerManager() {
    }

    public DevourerManager(Executor pExecutor) {
        mExecutor = pExecutor;
    }

    public Devourer of (Object key) {
        return mDevourerMap.computeIfAbsent(key, o -> new Devourer(key, mExecutor, this
        ));
    }

    public Devourer offer (Object key, Runnable fn){
        return mDevourerMap.computeIfAbsent(key, o -> new Devourer(key, mExecutor, this)).offer(fn);
    }

    public void remove (Object key) {
        mDevourerMap.remove(key);
    }
}
