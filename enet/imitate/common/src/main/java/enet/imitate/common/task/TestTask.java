package enet.imitate.common.task;

import enet.imitate.common.Log;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Description TODO  </br>
 * @Author tonywang</br>
 * @DATE 4/11/2019 10:59 AM</br>
 */
public class TestTask {

        static {
            Log.init(null);
        }
    public static void main(String[] args){
        TaskContext context = new TaskContext();
        AtomicInteger count = new AtomicInteger(0);
        TaskWrapper task = TaskWrapper.of(() -> System.out.println("count : " + count.getAndIncrement()));

        Step noOutStep = new Step.NoOutStep(task, pO -> System.out.println("noOutStep count : " + count.getAndIncrement()));
        Step closureStep = new Step.ClosureStep(task, () -> System.out.println("closureStep count : " + count.getAndIncrement()), () -> noOutStep);
        Step noInputSetp = new Step.NoInputSetp(task, () -> count.getAndIncrement(), c -> closureStep);
        Step retryStep = new Step.RetryStep(task, pO -> count.getAndIncrement(), p1 -> count.getAndIncrement(), p2 -> noInputSetp);
        task.currentStep(retryStep);
        context.inWaitingQueue(task);
        context.start();
    }
}
