package enet.imitate.common.task;

import javafx.concurrent.Task;
import javafx.scene.paint.Stop;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * 执行步骤/执行节点
 * 每个 Step 通过 {@link #next()} 顺序关联
 * 核心方法: {@link #run()}
 * @param <I>  输入参数类型
 * @param <R>  步骤返回值类型
 */
public class Step<I, R> {
    /**
     * 执行步骤 的名称
     */
    private   String            name;
    /**
     * 执行步骤 的说明
     */
    private   String            description;
    /**
     * 开始执行时间
     */
    private   long              startTime;
    /**
     * 执行时所需的参数
     */
    protected I                 param;
    /**
     * 保存执行结果.
     */
    protected R                 result;
    /**
     * 执行次数
     */
    protected AtomicInteger count = new AtomicInteger(0);
    /**
     * 执行体(当前Step的主要执行逻辑)
     */
    protected Function<I, R> fn;
    /**
     * 下一个 {@link Step} 判断的函数
     * 返回下一个 执行的 {@link Step}
     */
    protected Function<R, Step> nextStepFn;
    /**
     * 如果为true 则 暂停下一个步骤
     */
    private   boolean           suspendNext;
    /**
     * 所属Task
     */
    protected final TaskWrapper       task;
    /**
     * Step 唯一标识
     */
    protected       String            key;

    public Step(TaskWrapper pTask, Function<I, R> pFn, Function<R, Step> pNextStepFn) {
        this.task = pTask;
        this.fn = pFn;
        this.nextStepFn = pNextStepFn;
    }

    /**
     * nextStep 默认指向 StopStep
     * @param task
     * @param fn
     */
    public Step (TaskWrapper pTask, Function<I, R> pFn) {
        this(pTask, pFn, r-> pTask.stopStep());// 默认 指向 StopStep
    }

    public Step(TaskWrapper pTask) {
        this(pTask, null);
    }

    /**
     * 不用 输入参数的 Step
     * @param <R>
     */
    public static class NoInputSetp<R> extends Step<Void, R> {

        public NoInputSetp(TaskWrapper pTask, Supplier<R> pFn, Function<R, Step> pNextStepFn) {
            super(pTask, aVoid -> pFn.get(), pNextStepFn);
        }

        public NoInputSetp(TaskWrapper pTask, Function<Void, R> pFn) {
            super(pTask, pFn);
        }

        public NoInputSetp(TaskWrapper pTask) {
            super(pTask);
        }
        @Override
        public Step setParam(Void param) {
            throw new UnsupportedOperationException("NoInputStep not should have input param");
        }

    }

    /**
     * 不用 输出参数的 Step
     * @param <I>
     */
    public static  class NoOutStep<I> extends Step<I, Void> {

        public NoOutStep(TaskWrapper pTask, Consumer<I> pFn, Function<Void, Step> pNextStepFn) {
            super(pTask, i-> {
                pFn.accept(i);
                return null;
            }, pNextStepFn);
        }

        public NoOutStep(TaskWrapper pTask, Consumer<I> pFn) {
            super(pTask, i-> {
                pFn.accept(i);
                return null;
            });
        }

        public NoOutStep(TaskWrapper pTask) {
            super(pTask);
        }
    }

    /**
     * 任务Task 停止Step,也是最后一个Step
     * @param <I>
     */
    protected  static class StopStep<I> extends NoOutStep<I> {

        public StopStep(TaskWrapper pTask, Consumer<I> pFn, Function<Void, Step> pNextStepFn) {
            super(pTask, pFn, pNextStepFn);
        }

        public StopStep(TaskWrapper pTask, Consumer<I> pFn) {
            super(pTask, pFn);
        }

        public StopStep(TaskWrapper pTask) {
            super(pTask);
        }

        @Override
        protected Step next() {
            return null;
        }

        @Override
        public Step setNextStepFn(Function<Void, Step> nextStepFn) {
            throw new UnsupportedOperationException("StopStep not need nextStepFn");
        }
    }

    /**
     * 封闭执行的 Task, 没有输入参数, 也没有返回值
     */
    public static class ClosureStep extends Step<Void, Void>{
        public ClosureStep(TaskWrapper pTask, Runnable pFn, Supplier<Step> pNextStepFn) {
            super(pTask, aVoid -> {
                pFn.run();
                return null;
            }, aVoid -> pNextStepFn.get());
        }

        public ClosureStep(TaskWrapper pTask, Consumer<Void> pFn) {
            super(pTask, aVoid -> {
                pFn.accept(null);
                return null;
            });
        }

        public ClosureStep(TaskWrapper pTask) {
            super(pTask);
        }
    }

    /**
     * 可重复执行的Step
     * @param <I>
     * @param <R>
     */
    public static class RetryStep<I, R> extends Step<I, R> {
        /**
         * 重试时, 须手动设置为 false
         */
        private boolean complete;
        private Function<I, R> retryFn;
        private Function<RetryStep, I> paramFn;

        public RetryStep(TaskWrapper pTask, Function<I, R> pFn, Function<I, R> pRetryFn, Function<R, Step> pNextStepFn) {
            super(pTask, pFn, pNextStepFn);
            this.retryFn = pRetryFn;
        }

        public RetryStep(TaskWrapper pTask, Function<I, R> pFn) {
            super(pTask, pFn);
        }

        public RetryStep(TaskWrapper pTask) {
            super(pTask);
        }

        @Override
        protected void process(){
            count.getAndIncrement();
            if (count.get() > 1) {
                if (retryFn == null) throw new NullPointerException(getKey() + ": retryFn is null");
                if (paramFn != null) param = paramFn.apply(this); //重新计算输入的参数
                result = retryFn.apply(param);
            } else {
                if (fn == null) throw new NullPointerException(getKey() + ": fn is null");
                result = fn.apply(param);
            }
            complete = true;
        }

        public RetryStep setRetryFn(Function<I, R> retryFn) {
            this.retryFn = retryFn;
            return this;
        }

        public Function<I, R> getRetryFn() {
            return retryFn;
        }

        @Override
        public boolean isComplete() {
            return complete;
        }

        public RetryStep setComplete(boolean complete) {
            this.complete = complete;
            return this;
        }

        /**
         * 重新执行
         */
        public final void reRun() {
            complete = false;
            //  task.currentStep(): 即被暂停的Step 有可能 不等于 this.
            // 比如: 一个Task 中有 Step1, Step2, 当 Step2调了suspendNext(), 但之后Step1 调了此方法即ReRun().
            // 那么, 当Step1 reRun 执行完后Step2 却是 suspendNext 状态. 所以 这里, 要先把 被暂停的Step 的 suspendNext 设置为false
            if (task.currentStep() != this) task.currentStep().suspendNext = false;
            task.currentStep(this);
            if (isWaitingNext()) continueNext();
            else task.trigger();
        }
        /**
         * 参数获取函数,
         * 因为RetryStep可能每次的参数不一样
         * @param paramFn
         * @return
         */
        public RetryStep paramFn(Function<RetryStep, I> paramFn) {
            this.paramFn = paramFn;
            param = paramFn.apply(this);
            return this;
        }

    }

    /**
     * 调用执行函数
     */
    protected void process() {
        if (fn == null) throw new NullPointerException(getKey() + ": fn is null");
        // 如果已执行过了 则直接返回
        if (count.incrementAndGet() > 1) {
            task.mLog.error("Step被重复执行, Task被强制Stop!!!");
            task.shouldStop();
            return;
        }
        result = fn.apply(param);
        if (this instanceof StopStep) {
            task.stopped.compareAndSet(false, true);
        }
    }
    /**
     * 挂起/暂停
     * 等待
     */
    public final void suspendNext() {
        suspendNext = true;
    }

    /**
     * 恢复执行
     */
    public final void continueNext() {
        suspendNext = false;
        task.trigger();
    }

    public String getKey() {
        if (key == null) {
            key = task.getKey() + " Step: " + name + "";
        }
        return key;
    }


    public Step setParam(I param) {
        this.param = param;
        return this;
    }

    /**
     * Step执行
     * @return 下一个执行Step
     */
    public final Step run() {
        startTime = System.currentTimeMillis();
        process();
        return next();
    }

    /**
     * @return 如果 返回 null , 应该是 任务结束 或 应该是 任务暂停
     */
    protected Step next(){
     if (task.isShouldStop()) return task.stopStep();
     if (suspendNext) return null;
     return (nextStepFn == null ? null : nextStepFn.apply(result));
    }

    public Step setNextStepFn(Function<R, Step> nextStepFn) {
        this.nextStepFn = nextStepFn;
        return this;
    }

    /**
     * @return 如果 返回 null , 应该是 任务结束 或 应该是 任务暂停
     */
    public Function<R, Step> getNextStepFn() {
        return nextStepFn;
    }

    public Step setFn(Function<I, R> fn) {
        this.fn = fn;
        return this;
    }

    public String getName() {
        return name;
    }

    public Step setName(String pName) {
        name = pName;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Step setDescription(String pDescription) {
        description = pDescription;
        return this;
    }

    public long getStartTime() {
        return startTime;
    }

    public Step setStartTime(long pStartTime) {
        startTime = pStartTime;
        return this;
    }

    public I getParam() {
        return param;
    }

    public R getResult() {
        return result;
    }

    public Step setResult(R pResult) {
        result = pResult;
        return this;
    }

    public AtomicInteger getCount() {
        return count;
    }

    public Step setCount(AtomicInteger pCount) {
        count = pCount;
        return this;
    }

    public Function<I, R> getFn() {
        return fn;
    }

    /**
     * 是否被 暂停 中
     * @return
     */
    public boolean isWaitingNext() {
        return suspendNext;
    }

    public Step setSuspendNext(boolean pSuspendNext) {
        suspendNext = pSuspendNext;
        return this;
    }

    public TaskWrapper getTask() {
        return task;
    }



    public boolean isComplete() {
        return count.get() >= 1;
    }
}
