package enet.eureka.server;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import sun.misc.BASE64Decoder;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.Base64;

/**
 * @version V1.0  </br>
 * @ClassName TestController </br>
 * @Description TODO  </br>
 * @Author tonywang</br>
 * @DATE 3/27/2019 2:26 PM</br>
 */
@RestController
public class TestController {
    final Base64.Decoder decoder = Base64.getDecoder();
    final Base64.Encoder encoder = Base64.getEncoder();

    @RequestMapping(value = "/test", method = RequestMethod.POST)
    public String say() throws UnsupportedEncodingException {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        String st1 = request.getHeader("Authorization");
        System.out.println("Authorization :" + st1);
        System.out.println("decoder text: " + new String(decoder.decode(st1),"utf-8"));
        return new String(decoder.decode(st1),"utf-8");
    }
}
